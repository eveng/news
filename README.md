# Syllabus and News for CS594/690: Evidence Engineering

# May 3
  1. 12-1PM: Class lunch at Yassin's Falafel, 706 Walnut St, Knoxville, TN 37902


# Apr 28 
  1. [Motivation for the reports](https://bitbucket.org/eveng/presentations/src/master/motivation.md)
  1. Reports due on May 10, please commit code/document to 
    https://bitbucket.org/eveng/reports/
    in a separate folder named after your team.

# Apr 26
  1. [Text analysis](https://bitbucket.org/eveng/presentations/src/master/lsi-lda.w2v.md)
   


# Apr 20
 1. [Using graph database](https://bitbucket.org/eveng/tasks/src/master/neo4j.md)
 1. [Updates to results](https://bitbucket.org/eveng/tasks/src/master/results.md)

# Apr 12. The graphs have been constructed! :
 1. [method](https://bitbucket.org/eveng/tasks/src/master/graph.md)
 1. [file to file ](https://bitbucket.org/eveng/tasks/src/master/file-to-file.md)
 1. [hash-to-hash](https://bitbucket.org/eveng/tasks/src/master/results.md)
 1. [project-to-project](https://bitbucket.org/eveng/tasks/src/master/results.md)
 1. [person-to-project](https://bitbucket.org/eveng/tasks/src/master/results.md)
 1. [left-pad-related](https://bitbucket.org/eveng/tasks/src/master/left-pad.md)

# Feb 18
 - Progress on the tasks

# Feb 16  
  - Guest Lecture
    - Pat Tendick
    - Decisions as a service for application centric real time analytics
  - [Lecture on docker swarm: try at home](https://bitbucket.org/eveng/tasks/src/184567b73002d25677fd94b6c9ce8db811d2bbc0/dockerSwarm.md?at=master&fileviewer=file-view-default)
    - Python library for interacting with Consul: https://github.com/cablehead/python-consul

# Feb 11
 - Continuing presentations
 - Progress on tasks

# Feb 9
[More details on class project](https://bitbucket.org/eveng/tasks/src/3c89b46bddafff52bffb30619ba29b3b0caaa91f/README.md?at=master&fileviewer=file-view-default)

# Feb 4
 - login to your docker container on da2: ssh -p yourport netid@da2.eecs.utk.edu
```
9036:tdey2
9041:yma28
9016:jms
9027:nwilder
9047:szhong4
9048:mahmadza
9049:samreen
9050:jduggan1
9031:rhoque
9046:ahota
9053:lwan1
9052:ylu20
9051:yli118
```
 - read docker tutorial https://blog.talpor.com/2015/01/docker-beginners-tutorial/
 - create yor project on dockerhub
 - login to da2 netid@da2.eecs.utk.edu
 - run a docker container on da2
 - run docker engine in in docker: https://blog.docker.com/2013/09/docker-can-now-run-within-docker/
 - build a docker file for yarn cluster http://lresende.blogspot.com/2015/01/building-yarn-cluster-using-dockerio.html
# Feb 2
 - Continue presentation on perspectives paper and the two remaining papers

# Jan 28
 - Continue presentation on perspectives paper and the two remaining papers

# Jan 26 We may also continue the presentations
 - Class time to discuss potential projects
 - [Brainstorming results on data/project selection](https://docs.google.com/a/utk.edu/document/d/1OQoki82394pDT4zgcba6TcWRhZoscv_jUT5FgUFDbUY/edit?usp=sharing)

# Jan 21 [Todays Presentation](https://bitbucket.org/EvEng/papers/raw/master/VCSBigData.pdf)
  - We discussed data sources
  - Had started a presentation on the perspectives paper

# Jan 19 [Presentation](https://bitbucket.org/EvEng/papers/raw/master/class1.pdf)

# Jan 14 [Presentation](https://bitbucket.org/EvEng/papers/raw/master/class1.pdf)

[Syllabus](https://bitbucket.org/EvEng/news/raw/master/ee.pdf)